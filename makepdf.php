<?php
include ("./fpdf/fpdf.php");
$db = new PDO('mysql:host=localhost;dbname=php_crud','root','');

class mypdf extends FPDF
{
	function headerTable()
	{
		$this->SetFont('Times','B',12);
		$this->Cell(20,10,'ID',1,0,'C');
		$this->Cell(40,10,'First Name',1,0,'C');
		$this->Cell(60,10,'Last Name',1,0,'C');
		$this->Cell(80,10,'Age',1,0,'C');
		$this->Cell(100,10,'Contact',1,0,'C');
		$this->Ln();

	}
	function viewTable($db){
		$this->SetFont('Times','B',12);
		$stmt = $db->query("select * from members");
		while($data = $stmt->fetch(PDO::FETCH_OBJ)){
			$this->Cell(20,10,$data->id,1,0,'C');
			$this->Cell(40,10,$data->fname,1,0,'C');
			$this->Cell(60,10,$data->lname,1,0,'C');
			$this->Cell(80,10,$data->age,1,0,'C');
			$this->Cell(100,10,$data->contact,1,0,'C');
			$this->Ln();
		}
	}

	
}

$pdf = new mypdf();
$pdf->AliasNbPages();
$pdf->AddPage('L','A4',0);
$pdf->headerTable();
$pdf->viewTable($db);
$pdf->Output();
?>