<?php require_once 'php_action/db_connect.php'; ?>

<?php

if(isset($_POST['search']))
{
    $valueToSearch = $_POST['valueToSearch'];
    // search in all table columns
    // using concat mysql function
    $query = "SELECT * FROM `members` WHERE CONCAT(`id`, `fname`, `lname`, `contact` , `age`) LIKE '%".$valueToSearch."%'";
    $search_result = filterTable($query);
    
}
 else {
    $query = "SELECT * FROM `members`";
    $search_result = filterTable($query);
}

// function to connect and execute the query
function filterTable($query)
{
    $connect = mysqli_connect("localhost", "root", "", "php_crud");
    $filter_Result = mysqli_query($connect, $query);
    return $filter_Result;
}

?>


<!DOCTYPE html>
<html>
<head>
	<title>PHP CRUD</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

	<style type="text/css">
		body {
			width: 50%;
			margin: auto;
		}

		.row{
			margin-left: 15%;
		}

		table {
			width: 100%;
			margin-top: 20px;
		}
		
		h1{
			color: #FF6347;
			font-size: 30px;
		}
		
		.space{
				margin-left: 200px;
		}

		th{
			background-color: #BDB76B;
		}

		th,td{
				text-align: center;
					
		}

	</style>

</head>
<body>
<h1><center>Corporate Coders Task</h1><br>
<div class="manageMember">
	<div class="row">
		<a href="create.php"><button type="button" class="btn btn-dark">Add Member</button></a>
		<form action="index.php" method="post">
			<span class="space"><input type="text" name="valueToSearch" placeholder="Value To Search"></span>
	        <input type="submit" name="search" class="btn btn-dark" value="Filter">
    </div>
	<table border="1" cellspacing="0" cellpadding="0">
		<thead>
			<tr>
				<th>Name</th>
				<th>Age</th>
				<th>Contact</th>
				<th>Option</th>
			</tr>
		</thead>
	 <?php while($row = mysqli_fetch_array($search_result)):?>
		<tbody>
			<?php
			$sql = "SELECT * FROM members WHERE active = 1";
			$result = $connect->query($sql);

			if($result->num_rows > 0) {
				
					echo "<tr>
						<td>".$row['fname']." ".$row['lname']."</td>
						<td>".$row['age']."</td>
						<td>".$row['contact']."</td>
						<td>
							<a href='update.php?id=".$row['id']."'><button type='button'>Update</button></a>
							<a href='remove.php?id=".$row['id']."'><button type='button'>Delete</button></a>

						</td>
					</tr>";
				
			} else {
				echo "<tr><td colspan='5'><center>No Data Avaliable</center></td></tr>";
			}
			?>
		</tbody>
			<?php endwhile; ?> 
	</table></form><br><br>
	<a href='makepdf.php?id=".$row['id']."'><center><button type='button' class="btn btn-success">Generate PDF</button></center></a>
</div>

</body>
</html>